interface ActivePoint {
    node : SuffixNode,
    edge : string,
    length : number
}

interface SuffixEdge {
    start : number,
    end : number,
    node : SuffixNode
}

interface SuffixNode {
    parent : SuffixNode,
    edges : {[keys : string] : SuffixEdge},
    index : [number, number],
    terminals : boolean[],
    link : SuffixNode
}

interface DFSNode {
    node : SuffixNode,
    length : number
}

export default class SuffixTree {
    private text : string[] = [];
    private root : SuffixNode = this.newNode(undefined);
    private activePoint : ActivePoint = {
        node : this.root,
        edge : undefined,
        length : 0
    };
    private lastString() : string {
        return this.text[this.text.length - 1];
    }
    private newNode(parent : SuffixNode) : SuffixNode {
        return {
            parent : parent,
            edges : {},
            index : undefined,
            terminals : [],
            link : undefined
        }
    }
    private newEdge(start : number) : void {
        this.activeNode().edges[this.lastString()[start]] = {
            start : start,
            end : undefined,
            node : this.newNode(this.activeNode())
        };
    }
    private activeNode() : SuffixNode {
        return this.activePoint.node;
    }
    private activeEdge() : SuffixEdge {
        return this.activeNode().edges[this.activePoint.edge];
    }
    private walkDown() : void {
        let idx : number;
        if (this.activePoint.length && this.activeEdge().end == this.activeEdge().start + (this.activePoint.length - 1)){
            this.activePoint = {
                node : this.activeEdge().node,
                edge : undefined,
                length : 0
            }
        }
    }
    private nextChar() : string {
        return this.lastString()[this.activeEdge().start + this.activePoint.length];
    }
    private newBranch(follower : SuffixNode, at : number) : SuffixNode {
        // will cut the active edge and place a new node within it
        // get the leaf node at the end of the edge to be cut off
        let oldLeaf : SuffixNode = this.activeEdge().node;
        // create a new internal node that will cut off that edge
        let newIntern : SuffixNode = this.newNode(this.activeNode());
        // create a new leaf node that will branch the internal node
        let newLeaf : SuffixNode = this.newNode(newIntern);
        // attach the new node to the active node (which detaches from the oldLeaf)
        this.activeNode().edges[this.activePoint.edge] = {
            start : this.activeEdge().start,
            end : this.activeEdge().start + (this.activePoint.length - 1),
            node : newIntern
        };
        // give that new internal node an edge to the old leaf node and reset parent
        newIntern.edges[this.lastString()[this.activeEdge().start + this.activePoint.length]] = {
            start :this.activeEdge().start + this.activePoint.length,
            end : undefined,
            node : oldLeaf
        };
        oldLeaf.parent = newIntern;
        // give that new internal node an edge to the new leaf node
        newIntern.edges[this.lastString()[at]] = {
            start : at,
            end : undefined,
            node : newLeaf
        };
        follower && (follower.link = newIntern);
        return newIntern;
    }
    private newTerminus(follower : SuffixNode) : SuffixNode {
        // will cut the active edge and place a new node within it
        // get the leaf node at the end of the edge to be cut off
        let oldLeaf : SuffixNode = this.activeEdge().node;
        // create a new internal node that will cut off that edge
        let newIntern : SuffixNode = this.newNode(this.activeNode());
        newIntern.terminals[this.text.length -1] = true;
        // attach the new node to the active node (which detaches from the oldLeaf)
        this.activeNode().edges[this.activePoint.edge] = {
            start : this.activeEdge().start,
            end : this.activeEdge().start + (this.activePoint.length - 1),
            node : newIntern
        };
        // give that new internal node an edge to the old leaf node and reset parent
        newIntern.edges[this.lastString()[this.activeEdge().start + this.activePoint.length]] = {
            start :this.activeEdge().start + this.activePoint.length,
            end : undefined,
            node : oldLeaf
        };
        oldLeaf.parent = newIntern;
        follower && (follower.link = newIntern);
        return newIntern;
    }
    constructor(){
    }
    public add(text : string) : SuffixTree {
        this.text.push(text);
        let follower : SuffixNode;
        // perform the first phase explicitely since it is always the same and simple
        //this.newEdge(0);
        // track the number of existing leaves in order to perform that many rule 1s implicitely at the beginning of a phase
        let leafs : number = 0;
        // for each phase
        for (let i : number = 0; i < text.length; i++){
            // for each extension less the implicit rule 1s at the start
            for (let j : number = leafs; j <= i; j++){
                this.walkDown();
                if (!this.activePoint.length){
                    this.activePoint.edge = text[i];
                    if (this.activeEdge()){
                        this.activePoint.length++;
                        break;
                    } else {
                        follower && (follower.link = this.activeNode());
                        follower = undefined;
                        this.newEdge(i);
                        leafs++;
                        if (this.activeNode().link){
                            this.activePoint.node = this.activeNode().link;
                        }
                    }
                } else {
                    if (this.nextChar() == text[i]){
                        this.activePoint.length++;
                        break;
                    } else {
                        follower = this.newBranch(follower, i);
                        leafs++;
                        if (this.activeNode().link){
                            this.activePoint.node = this.activeNode().link;
                        } else {
                            this.activePoint.edge = text[this.activeEdge().start + 1];
                            this.activePoint.length--;
                        }
                    }
                }
            }
        }
        for (let i : number = leafs; i <= text.length; i++){
            //handle terminating character, not sure yet how
            this.walkDown();
            if (!this.activePoint.length){
                follower && (follower.link = this.activeNode());
                follower = undefined;
                this.activeNode().terminals[this.text.length - 1] = true;
                if (this.activeNode().link){
                    this.activePoint.node = this.activeNode().link;
                }
            } else {
                follower = this.newTerminus(follower);
                if (this.activeNode().link){
                    this.activePoint.node = this.activeNode().link;
                } else {
                    this.activePoint.edge = text[this.activeEdge().start + 1];
                    this.activePoint.length--;
                }
            }
        }
        // DFS to set indices
        let stack : DFSNode[] = [{node : this.root, length : 0}];
        let thisNode : DFSNode;
        let thisEdge : SuffixEdge;
        while(stack.length){
            thisNode = stack.pop();
            for (let edge in thisNode.node.edges){
                thisEdge = thisNode.node.edges[edge];
                if (thisEdge.end === undefined){
                    thisEdge.end = text.length - 1;
                    thisEdge.node.index = [this.text.length - 1, thisEdge.start - thisNode.length];
                } else {
                    if (thisEdge.node.terminals[this.text.length - 1]){
                        thisEdge.node.index = [this.text.length - 1, text.length - ((thisEdge.end - thisEdge.start + 1) + thisNode.length)];
                    }
                    stack.push({
                        node : thisEdge.node,
                        length : (thisEdge.end - thisEdge.start + 1) + thisNode.length
                    })
                }
            }
        }
        return this;
    }
}